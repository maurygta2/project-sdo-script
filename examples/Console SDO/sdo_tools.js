/*jshint maxerr: 10000000 */

String.prototype._remove = function() {
    return this.replace(/(")/g,"");
};

function sdo(param) {
    this.functions = {
        And : function(param_) {
            f2 = false;
            for (let f1 in param_) {
                if (param_[0] == param_[f1]) {
                     f2 = true;
                } else {
                    f2 = false;
                    return f2;
                }
            }
            return f2;
        },
        Array_ : (param_) => {
            f2 = "";
            f3 = [];
            f4 = 0;
            f5 = 0;
            for (f1=0;f1 < param_.length;f1++) {
                if (param_[f1] == '"') {
                    f5 += 1;
                }
                if (param_[f1] == "(" && f5%2 === 0) {
                    f4 += 1;
                }
                if (param_[f1] == ")" && f5%2 === 0) {
                    f4 -= 1;
                }
                if (param_[f1] != ",") {
                    f2 += param_[f1];
                    if (f1 == param_.length-1) {
                        f3.push(f2);
                    }
                } else if (f4 === 0 && f5%2 === 0) {
                    f3.push(f2);
                    f2 = "";
                } else {
                    f2 += ",";
                }
            }
            return f3;
        },
        format_f: function(param_,param_2) {
            f0 = param_.replace(param_2,"");
            f1 = f0.substring(0,f0.length-1);
            return this.Array_(f1);
        }
    };
    this.tools = {
        Date: (param_) => {
            if (param_[0].search('"') === 0) {
                f0 = new Date();
                f1 = param_[0]._remove();
                if (f1 == "hours") {
                    return f0.getHours();
                }
                if (f1 == "date") {
                    return f0.getDate();
                }
                if (f1 == "day") {
                    return f0.getDay()+1;
                }
                if (f1 == "year") {
                    return f0.getFullYear();
                }
                if (f1 == "ms") {
                    return f0.getMilliseconds();
                }
                if (f1 == "minutes") {
                    return f0.getMinutes();
                }
                if (f1 == "month") {
                    return f0.getMonth();
                }
                if (f1 == "s") {
                    return f0.getSeconds();
                }
                if (f1 == "toString") {
                    return f0.toString();
                }
            }
        },
        Id : 0,
        Math: {
            pow: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    return Math.pow(Number(param_[0]._remove()),Number(param_[1]._remove()));
                }
            },
            round: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.round(Number(param_[0]._remove()));
                }
            },
            PI: Math.PI,
            abs: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.abs(Number(param_[0]._remove()));
                }
            },
            cbrt: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.cbrt(Number(param_[0]._remove()));
                }
            },
            ceil: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.ceil(Number(param_[0]._remove()));
                }
            },
            floor: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.floor(Number(param_[0]._remove()));
                }
            },
            sqrt: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.sqrt(Number(param_[0]._remove()));
                }
            },
            trunc: (param_) => {
                if (param_[0].search('"') === 0) {
                    return Math.trunc(Number(param_[0]._remove()));
                }
            },
            max: (param_) => {
                f1 = [];
                for (let f0 in param_) {
                    if (param_[f0].search('"') === 0) {
                        f1.push(param_[f0]._remove());
                    }
                }
                return Math.max.apply(null,f1);
            },
            min: (param_) => {
                f1 = [];
                for (let f0 in param_) {
                    if (param_[f0].search('"') === 0) {
                        f1.push(param_[0]._remove());
                    }
                }
                return Math.min.apply(null,f1);
            },
            MF_1: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    return Math.floor((param_[0]._remove()*100)*(1+(param_[1]._remove())*0.01))/100;
                }
            },
            MF_2: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    return Math.floor((param_[0]._remove()*100)/(1-(param_[1]._remove()*0.01)))/100;
                }
            },
            MF_3: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    return Math.floor((param_[0]._remove()*100)*(1-(param_[1]._remove()*0.01)))/100;
                }
            },
            MF_4: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    return Math.floor((param_[0]._remove()*100)/(1+(param_[1]._remove()*0.01)))/100;
                }
            },
            MF_5: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    return ((param_[1]._remove()*100)/param_[0]._remove())-100;
                }
            },
            Arith: (param_) => {
                f1 = [];
                for (let f0 in param_) {
                    if (param_[f0].search('"') === 0) {
                        f1.push(param_[f0]._remove());
                    }
                }
                f3;
                for (let f2 in f1) {
                    if (f2 === "0") {
                        f3 = Number(f1[0]);
                        continue;
                    }
                    if (f2%2 == 1) {
                        f4 = Number(f1[Number(f2) + 1]);
                        if (f1[f2] == "+") {
                            f3 += f4;
                            continue;
                        }
                        if (f1[f2] == "-") {
                            f3 -= f4;
                            continue;
                        }
                        if (f1[f2] == "/") {
                            f3 /= f4;
                            continue;
                        }
                        if (f1[f2] == "*") {
                            f3 *= f4;
                            continue;
                        }
                        if (f1[f2] == "%") {
                            f3 %= f4;
                            continue;
                        }
                    }
                }
                return f3;
            }
        },
        Volume: {
            L: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Ml") {
                        return Math.floor((f0*1000)*0.001)/1000;
                    }
                    if (f1 == "CB_m") {
                        return f0*1000;
                    }
                }
            },
            Ml: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "L") {
                        return f0*1000;
                    }
                    if (f1 == "CB_m") {
                        return f0*1000000;
                    }
                }
            },
            CB_m: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Ml") {
                        return Math.floor((f0*1000000)*0.000001)/1000000;
                    }
                    if (f1 == "L") {
                        return Math.floor((f0*1000)*0.001)/1000;
                    }
                }
            }
        },
        Length: {
            Nm: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "MCm") {
                        return Math.floor((f0*1000)*0.001)/1000;
                    }
                    if (f1 == "Mm") {
                        return Math.floor((f0*1000000)*0.000001)/1000000;
                    }
                    if (f1 == "Cm") {
                        return Math.floor((f0*10000000)*0.0000001)/10000000;
                    }
                    if (f1 == "M") {
                        return Math.floor((f0*1000000000)*0.000000001)/1000000000;
                    }
                    if (f1 == "Km") {
                        return Math.floor((f0*1000000000000)*0.000000000001)/1000000000000;
                    }
                }
            },
            MCm: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Nm") {
                        return f0*1000;
                    }
                    if (f1 == "Mm") {
                        return Math.floor((f0*1000)*0.001)/1000;
                    }
                    if (f1 == "Cm") {
                        return Math.floor((f0*10000)*0.0001)/10000;
                    }
                    if (f1 == "M") {
                        return Math.floor((f0*1000000)*0.000001)/1000000;
                    }
                    if (f1 == "Km") {
                        return Math.floor((f0*1000000000)*0.000000001)/1000000000;
                    }
                }
            },
            Mm: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Nm") {
                        return f0*1000000;
                    }
                    if (f1 == "MCm") {
                        return f0*1000;
                    }
                    if (f1 == "Cm") {
                        return Math.floor((f0*10)*0.1)/10;
                    }
                    if (f1 == "M") {
                        return Math.floor((f0*1000)*0.001)/1000;
                    }
                    if (f1 == "Km") {
                        return Math.floor((f0*1000000)*0.000001)/1000000;
                    }
                }
            },
            Cm: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Nm") {
                        return f0*10000000;
                    }
                    if (f1 == "MCm") {
                        return f0*10000;
                    }
                    if (f1 == "Cm") {
                        return f0*10;
                    }
                    if (f1 == "M") {
                        return Math.floor((f0*100)*0.01)/100;
                    }
                    if (f1 == "Km") {
                        return Math.floor((f0*100000)*0.00001)/100000;
                    }
                }
            },
            M: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Nm") {
                        return f0*1000000000;
                    }
                    if (f1 == "MCm") {
                        return f0*1000000;
                    }
                    if (f1 == "Mm") {
                        return f0*1000;
                    }
                    if (f1 == "Cm") {
                        return f0*100;
                    }
                    if (f1 == "Km") {
                        return Math.floor((f0*1000)*0.001)/1000;
                    }
                }
            },
            Km: (param_) => {
                if (param_[0].search('"') === 0 && param_[1].search('"') === 0) {
                    f0 = Number(param_[0]._remove());
                    f1 = param_[1]._remove();
                    if (f1 == "Nm") {
                        return f0*1000000000000;
                    }
                    if (f1 == "MCm") {
                        return f0*1000000000;
                    }
                    if (f1 == "Mm") {
                        return f0*1000000;
                    }
                    if (f1 == "Cm") {
                        return f0*100000;
                    }
                    if (f1 == "M") {
                        return f0*1000;
                    }
                }
            }
        },
        var_name: (param_) => {
            if (param_[0].search('"') === 0) {
                return obj_sdo[param_[0]._remove()];
            } 
        }
    };
    let obj_sdo = {};
    if (typeof(param) == "string") {
        let f0 = (param.indexOf("#start#") !== -1) ? param.indexOf("#start#")+7 : 0;
        let f1 = "";
        let f2 = false;
        let f3 = 0;
        let f4 = [];
        while (f0 < param.length) {
            if (param[f0] == "$" && f2 === false) {
                f1 += param[f0];
                f2 = true;
            } else if (f2 === true) {
                f1 += param[f0];
                if (param[f0] == '"') {
                    f3 += 1;
                }
            }
            if ((f3 - (Math.floor(f3/2)*2)) === 0 && param[f0] == ";") {
                f4.push(f1);
                f3 = 0;
                f2 = false;
                f1 = "";
            }
            f0 += 1;
        }
        let f5 = 0;
        let f6 = [];
        let f7 = [];
        while (f5 < f4.length) {
            let _f0 = f4[f5];
            f6[f5] = _f0.substring(_f0.indexOf("$")+1,_f0.indexOf(" "));
            f7[f5] = _f0.substring(_f0.indexOf("= ")+2,_f0.length-1);
            f5 += 1;
        }
        for (f8=0;f8 < f4.length;f8++) {
            if (f7[f8].indexOf('"') === 0 && f7[f8].lastIndexOf('"') === f7[f8].length-1) {
                _f1 = f7[f8]._remove().replace(/(#{a})/g,'"');
                obj_sdo[f6[f8]] = _f1;
                continue;
            }
            if (f7[f8].indexOf("#") === 0 && f7[f8].lastIndexOf("#") === f7[f8].length-1) {
                _f1 = f7[f8].replace("#","");
                _f2 = _f1.substring(0,_f1.length-1);
                _f3 = this.functions.Array_(_f2);
                _f5 = "";
                for (let _f4 in _f3) {
                    _f0 = _f3[_f4];
                    if (_f0.indexOf('"') === 0) {
                        __f0 = _f0._remove().replace(/(#{a})/g,'"');
                        _f5 += __f0;
                        continue;
                    }
                    if (_f0.indexOf("random()") === 0) {
                        _f5 += Math.random();
                        continue;
                    }
                    if (_f0.indexOf('PI()') === 0) {
                        _f5 += String(this.tools.Math.PI);
                        continue;
                    }
                    if (_f0.indexOf('Id()') === 0) {
                        _f5 += String(this.tools.Id);
                        this.tools.Id += 1;
                        continue;
                    }
                    if (_f0.indexOf('Date(') === 0) {
                        __f0 = this.functions.format_f(_f0,'Date(');
                        _f5 += String(this.tools.Date(__f0));
                    }
                    if (_f0.indexOf('pow(') === 0) {
                        __f0 = this.functions.format_f(_f0,'pow(');
                        _f5 += String(this.tools.Math.pow(__f0));
                        continue;
                    }
                    if (_f0.indexOf('round(') === 0) {
                        __f0 = this.functions.format_f(_f0,'round(');
                        _f5 += String(this.tools.Math.round(__f0));
                        continue;
                    }
                    if (_f0.indexOf('abs(') === 0) {
                        __f0 = this.functions.format_f(_f0,'abs(');
                        _f5 += String(this.tools.Math.abs(__f0));
                        continue;
                    }
                    if (_f0.indexOf('cbrt(') === 0) {
                        __f0 = this.functions.format_f(_f0,"cbrt(");
                        _f5 += String(this.tools.Math.cbrt(__f0));
                        continue;
                    }
                    if (_f0.indexOf('ceil(') === 0) {
                        __f0 = this.functions.format_f(_f0,"ceil(");
                        _f5 += String(this.tools.Math.ceil(__f0));
                        continue;
                    }
                    if (_f0.indexOf('floor(') === 0) {
                        __f0 = this.functions.format_f(_f0,'floor(');
                        _f5 += String(this.tools.Math.floor(__f0));
                        continue;
                    }
                    if (_f0.indexOf('sqrt(') === 0) {
                        __f0 = this.functions.format_f(_f0,"sqrt(");
                        _f5 += String(this.tools.Math.sqrt(__f0));
                        continue;
                    }
                    if (_f0.indexOf('trunc(') === 0) {
                        __f0 = this.functions.format_f(_f0,'trunc(');
                        _f5 += String(this.tools.Math.trunc(__f0));
                        continue;
                    }
                    if (_f0.indexOf('max(') === 0) {
                        __f0 = this.functions.format_f(_f0,'max(');
                        _f5 += String(this.tools.Math.max(__f0));
                        continue;
                    }
                    if (_f0.indexOf('min(') === 0) {
                        __f0 = this.functions.format_f(_f0,'min(');
                        _f5 += String(this.tools.Math.min(__f0));
                        continue;
                    }
                    if (_f0.indexOf('MF_1(') === 0) {
                        __f0 = this.functions.format_f(_f0,'MF_1(');
                        _f5 += String(this.tools.Math.MF_1(__f0));
                        continue;
                    }
                    if (_f0.indexOf('MF_2(') === 0) {
                        __f0 = this.functions.format_f(_f0,'MF_2(');
                        _f5 += String(this.tools.Math.MF_2(__f0));
                        continue;
                    }
                    if (_f0.indexOf('MF_3(') === 0) {
                        __f0 = this.functions.format_f(_f0,'MF_3(');
                        _f5 += String(this.tools.Math.MF_3(__f0));
                        continue;
                    }
                    if (_f0.indexOf('MF_4(') === 0) {
                        __f0 = this.functions.format_f(_f0,'MF_4(');
                        _f5 += String(this.tools.Math.MF_4(__f0));
                        continue;
                    }
                    if (_f0.indexOf('MF_5(') === 0) {
                        __f0 = this.functions.format_f(_f0,'MF_5(');
                        _f5 += String(this.tools.Math.MF_5(__f0));
                        continue;
                    }
                    if (_f0.indexOf('Arith(') === 0) {
                        __f0 = this.functions.format_f(_f0,"Arith(");
                        _f5 += String(this.tools.Math.Arith(__f0));
                        continue;
                    }
                    if (_f0.indexOf('var_name(') === 0) {
                        __f0 = this.functions.format_f(_f0,'var_name(');
                        _f5 += String(this.tools.var_name(__f0));
                        continue;
                    }
                    if (_f0.indexOf('Volume.') === 0) {
                        if (_f0.indexOf("Volume.L(") === 0) {
                            __f0 = this.functions.format_f(_f0,"Volume.L(");
                            _f5 += String(this.tools.Volume.L(__f0));
                            continue;
                        }
                        if (_f0.indexOf('Volume.Ml(') === 0) {
                            __f0 = this.functions.format_f(_f0,"Volume.Ml(");
                            _f5 += String(this.tools.Volume.Ml(__f0));
                            continue;
                        }
                        if (_f0.indexOf("Volume.CB_m(") === 0) {
                            __f0 = this.functions.format_f(_f0,"Volume.CB_m(");
                            _f5 += String(this.tools.Volume.CB_m(__f0));
                            continue;
                        }
                        continue;
                    }
                    if (_f0.indexOf('Length.') === 0) {
                        if (_f0.indexOf('Length.Nm(') === 0) {
                            __f0 = this.functions.format_f(_f0,"Length.Nm(");
                            _f5 += String(this.tools.Length.Nm(__f0));
                            continue;
                        }
                        if (_f0.indexOf('Length.MCm(') === 0) {
                            __f0 = this.functions.format_f(_f0,"Length.MCm(");
                            _f5 += String(this.tools.Length.MCm(__f0));
                            continue;
                        }
                        if (_f0.indexOf('Length.Mm(') === 0) {
                            __f0 = this.functions.format_f(_f0,"Length.Mm(");
                            _f5 += String(this.tools.Length.Mm(__f0));
                            continue;
                        }
                        if (_f0.indexOf('Length.Cm(') === 0) {
                            __f0 = this.functions.format_f(_f0,"Length.Cm(");
                            _f5 += String(this.tools.Length.Cm(__f0));
                            continue;
                        }
                        if (_f0.indexOf('Length.M(') === 0) {
                            __f0 = this.functions.format_f(_f0,"Length.M(");
                            _f5 += String(this.tools.Length.M(__f0));
                            continue;
                        }
                        if (_f0.indexOf("Length.Km(") === 0) {
                            __f0 = this.functions.format_f(_f0,"Length.Km(");
                            _f5 += String(this.tools.Length.Km(__f0));
                            continue;
                        }
                        continue;
                    }
                }
                obj_sdo[f6[f8]] = _f5;
            }
        }
    }
    return obj_sdo;
}
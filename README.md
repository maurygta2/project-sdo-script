# Project SDO Script

Script simples e funcional pra guarda dados.

* Use $ para declarar variavel.
* Use "" sempre para declarar numero ou texto.
* Use ; para terminar a linha.
```php
$j = "2";
$login = #Date("ms")#;
$kan = "Clin";
```

Data Script use #method(),method2()#

| Methods | Return |
| ------ | ------ |
| "Hello World!" | String |
| random() | random 0-1 |
| Date("day") | day |
| Date("date") | date |
| Date("toString") | Return Data String |
| Date("month") | month |
| Date("year") | year |
| Date("ms") | milleseconds |
| Date("s") | seconds |
| Date("hours") | hours |
| Date("minutes") | minutes |
| PI() | 3.14 |
| pow(param,param2) | power value |
| round(param) | round value |